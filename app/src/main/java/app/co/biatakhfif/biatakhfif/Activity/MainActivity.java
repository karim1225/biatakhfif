package app.co.biatakhfif.biatakhfif.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import app.co.biatakhfif.biatakhfif.Class.G;
import app.co.biatakhfif.biatakhfif.Class.SharedPref;
import app.co.biatakhfif.biatakhfif.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    /*==========تعریف های لایه نمایش=========*/
    ConstraintLayout root;
    Toolbar toolbar;
    DrawerLayout drawer;
    TextView btn_sing_in;


    /*=============متغییر های این صفحه==============*/
    public static  String user_mob,exit;



    /*===================تغییر فونت==================*/
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    /*========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*===========فراخونی تابع ها===========*/
            castha();
            toolbar_void();
            clickha();




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_category) {
            // Handle the camera action
        } else if (id == R.id.nav_city) {

        } else if (id == R.id.nav_takhfif) {

        } else if (id == R.id.nav_food) {

        } else if (id == R.id.nav_digi) {

        } else if (id == R.id.nav_about) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*==================کست ها و کلیک ها=====================*/

    public void castha(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        SharedPref.init(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);

        btn_sing_in = (TextView) headerview.findViewById(R.id.btn_sing_in);
        user_mob =SharedPref.read("user_mob","null");
        if(user_mob.equals("null")){
            btn_sing_in.setText(getResources().getString(R.string.nav_vorod));
        }else {
            btn_sing_in.setText(SharedPref.read("user_name","")+" " + SharedPref.read("user_family",""));
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        root=(ConstraintLayout) findViewById(R.id.root);
    }

    public void clickha(){
        btn_sing_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_mob.equals("null")){
                    Intent intent = new Intent(G.context, Singin_Activity.class);
                    startActivity(intent);
                    drawer.closeDrawer(GravityCompat.START);
                    finish();
                }else {
                    Intent intent = new Intent(G.context, User_info_Activity.class);
                    startActivity(intent);
                    drawer.closeDrawer(GravityCompat.START);
                    finish();
                }

            }
        });
    }

    public void toolbar_void() {
        toolbar.setTitle(R.string.app_name_farsi);
        setSupportActionBar(toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setTextSize(16);
            }
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

    }

    /*=================نمایش دادن مسیج=========================*/
    public void show_msg(String msg_txt) {
        TSnackbar snackbar = TSnackbar.make(root, msg_txt, TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
