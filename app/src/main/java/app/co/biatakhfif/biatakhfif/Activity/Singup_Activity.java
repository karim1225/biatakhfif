package app.co.biatakhfif.biatakhfif.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import app.co.biatakhfif.biatakhfif.Class.G;
import app.co.biatakhfif.biatakhfif.Class.SharedPref;
import app.co.biatakhfif.biatakhfif.Model.Web_res_data;
import app.co.biatakhfif.biatakhfif.R;
import app.co.biatakhfif.biatakhfif.WebService.APIClient;
import app.co.biatakhfif.biatakhfif.WebService.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Singup_Activity extends AppCompatActivity {

    /*==========تعریف های لایه نمایش=========*/
    Toolbar toolbar;
    CoordinatorLayout root;
    ConstraintLayout panel_moshakhasat, panel_code_active, time_panel;
    EditText edt_user_name, edt_user_family, edit_user_mob, edit_user_pass, edt_pass_repit, edit_email, edit_active_code;
    Button btn_vorod, btn_send_code, btn_call_me_code;
    TextView min_txt, sec_txt;
    ProgressBar progress_bar;
    /*=============متغییر های این صفحه==============*/
    JSONObject data_json, data = null;
    public static String user_id, user_mob, user_name, user_family, user_email, active_code,user_pass;

    /*======متغیرهای تایمر=======*/
    int mintimer = 1;
    int sectimer = 30;
    int sh_therad = 0;
    public static Handler handler;

    /*=================تغییر فونت======================*/
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        castha();
        toolbar_void();
        clickha();
    }

    /*==============کست ها و کلیک ها==============*/
    public void toolbar_void() {
        toolbar.setTitle("ثبت نام");
        setSupportActionBar(toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setTextSize(16);
            }
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void castha() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        root = (CoordinatorLayout) findViewById(R.id.root);

        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        btn_vorod = (Button) findViewById(R.id.btn_vorod);
        btn_send_code = (Button) findViewById(R.id.btn_send_code);
        btn_call_me_code = (Button) findViewById(R.id.btn_call_me_code);

        edit_user_mob = (EditText) findViewById(R.id.edit_user_mob);
        edt_user_name = (EditText) findViewById(R.id.edt_user_name);
        edt_user_family = (EditText) findViewById(R.id.edt_user_family);
        edit_user_pass = (EditText) findViewById(R.id.edit_user_pass);
        edt_pass_repit = (EditText) findViewById(R.id.edt_pass_repit);
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_active_code = (EditText) findViewById(R.id.edit_active_code);

        min_txt = (TextView) findViewById(R.id.min_txt);
        sec_txt = (TextView) findViewById(R.id.sec_txt);

        panel_code_active = (ConstraintLayout) findViewById(R.id.panel_code_active);
        panel_moshakhasat = (ConstraintLayout) findViewById(R.id.panel_moshakhasat);
        time_panel = (ConstraintLayout) findViewById(R.id.time_panel);


        handler = new Handler();

    }
    public void clickha() {

        btn_vorod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_user_mob.getText().toString().equals("") || edt_user_name.getText().toString().equals("") || edit_user_pass.getText().toString().equals("") ||
                        edt_pass_repit.getText().toString().equals("") || edit_email.getText().toString().equals("")) {
                    show_msg(getResources().getString(R.string.msg_item_null));
                } else {
                    if(edit_user_pass.getText().length()< 8){
                        show_msg(getResources().getString(R.string.msg_pass_not_ok));
                    } else if (edit_user_mob.getText().length() < 11) {
                        show_msg(getResources().getString(R.string.msg_mob_not_ok));
                    }else if(! edit_email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")){
                        show_msg(getResources().getString(R.string.msg_email_not_ok));
                    }else {

                        if (edit_user_pass.getText().toString().equals(edt_pass_repit.getText().toString())) {
                            progress_bar.setVisibility(View.VISIBLE);
                            panel_moshakhasat.setVisibility(View.INVISIBLE);
                            send_sms(toBase64(data_json_cod_send_sms().toString()));

                        } else  {
                            show_msg(getResources().getString(R.string.msg_no_pass_repit));
                        }
                    }


                }


            }
        });

        btn_send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_active_code.getText().toString().equals("")) {
                    show_msg(getResources().getString(R.string.msg_active_code_null));
                } else {
                    if (edit_active_code.getText().toString().equals(active_code)) {
                        progress_bar.setVisibility(View.VISIBLE);
                        panel_code_active.setVisibility(View.INVISIBLE);
                        sing_up(toBase64(data_json_cod_sing_up().toString()));


                    } else {
                        show_msg(getResources().getString(R.string.msg_active_code_error));
                    }
                }
            }
        });

        btn_call_me_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress_bar.setVisibility(View.VISIBLE);
                panel_code_active.setVisibility(View.INVISIBLE);
                call_user(toBase64(data_json_cod_call_user().toString()));
            }
        });

    }
    /*=====================کدهای تایمر استفاده شده========================*/
    public void timer() {
        final Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (sh_therad == 0) {
                        Thread.sleep(1000);
                        if (sectimer == 0) {
                            if (mintimer != 0) {
                                mintimer--;
                                sectimer = 59;

                            } else {

                                sh_therad = 1;
                            }

                        } else {
                            sectimer--;
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (sh_therad == 1) {
                                    time_enable();
                                }

                                if (mintimer < 10) {
                                    min_txt.setText("0" + mintimer + "");
                                } else {
                                    min_txt.setText(mintimer + "");
                                }
                                if (sectimer < 10) {
                                    sec_txt.setText("0" + sectimer + "");
                                } else {
                                    sec_txt.setText(sectimer + "");
                                }
                            }
                        });
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        if (sh_therad == 0) {
            thread.start();
        }

    }
    public void time_enable() {
        btn_call_me_code.setVisibility(View.VISIBLE);
        time_panel.setVisibility(View.INVISIBLE);
    }
    /*====================اتصال به سرور=======================*/
    public void send_sms(String data) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.send_sms(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {

                if (response.isSuccessful()) {
                    JSONObject data = null;
                    String vall = fromBase64(response.body().getData());
                    try {
                        data = new JSONObject(vall);
                        active_code = data.get("code").toString();
                        progress_bar.setVisibility(View.INVISIBLE);
                        panel_code_active.setVisibility(View.VISIBLE);
                        timer();

                    } catch (JSONException e) {
                        //e.printStackTrace();
                        show_msg(getResources().getString(R.string.msg_error));
                    }


                } else if (response.code() == 300) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_user_mob_yes));

                } else if (response.code() == 301) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_email_yes));
                } else {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));
                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if (t instanceof IOException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                } else if (t instanceof TimeoutException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }

                progress_bar.setVisibility(View.INVISIBLE);
                panel_moshakhasat.setVisibility(View.VISIBLE);
                show_msg(getResources().getString(R.string.msg_error_res_data));
            }
        });
    }

    public void sing_up(String data) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.sing_up(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {

                if (response.isSuccessful()) {
                    JSONObject data = null;
                    String vall = fromBase64(response.body().getData());
                    try {
                        data = new JSONObject(vall);
                        user_id = data.get("id").toString();
                        SharedPref.write("user_id", user_id);

                        user_email = data.get("email").toString();
                        SharedPref.write("user_email", user_email);

                        user_name = data.get("user_name").toString();
                        SharedPref.write("user_name", user_name);

                        user_family = data.get("user_family").toString();
                        SharedPref.write("user_family", user_family);

                        user_pass = data.get("user_pass").toString();
                        SharedPref.write("user_pass", user_pass);

                        user_mob = data.get("user_mob").toString();
                        SharedPref.write("user_mob", user_mob);

                        Intent intent = new Intent(G.context, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } catch (JSONException e) {
                        //e.printStackTrace();
                        show_msg(getResources().getString(R.string.msg_error));
                    }


                } else if (response.code() == 300) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_user_mob_yes));

                } else if (response.code() == 301) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_email_yes));
                } else {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));

                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if (t instanceof IOException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                } else if (t instanceof TimeoutException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }else{
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));
                }


            }
        });

    }

    public void call_user(String data){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.call_user(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {

                if(response.isSuccessful()){
                    show_msg(getResources().getString(R.string.msg_call_user));
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_code_active.setVisibility(View.VISIBLE);
                    btn_call_me_code.setVisibility(View.INVISIBLE);
                    time_panel.setVisibility(View.VISIBLE);
                    timer();
                }else {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_code_active.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));
                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if (t instanceof IOException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_code_active.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                } else if (t instanceof TimeoutException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_code_active.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }

                progress_bar.setVisibility(View.INVISIBLE);
                panel_code_active.setVisibility(View.VISIBLE);
                show_msg(getResources().getString(R.string.msg_error_res_data));
            }
        });
    }

    /*=========================کد و دیکد کردن اطلاعات====================*/
    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String fromBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject data_json_cod_sing_up() {
        data_json = new JSONObject();
        try {
            data_json.put("user_mob", edit_user_mob.getText().toString());
            data_json.put("user_pass", edit_user_pass.getText().toString());
            data_json.put("user_name", edt_user_name.getText().toString());
            data_json.put("user_family", edt_user_family.getText().toString());
            data_json.put("email", edit_email.getText().toString());


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }

    public JSONObject data_json_cod_send_sms() {
        data_json = new JSONObject();
        try {
            data_json.put("user_mob", edit_user_mob.getText().toString());
            data_json.put("email", edit_email.getText().toString());


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }

    public JSONObject data_json_cod_call_user() {
        data_json = new JSONObject();
        try {
            data_json.put("user_mob", edit_user_mob.getText().toString());
            data_json.put("code", active_code);


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }


    /*=================نمایش دادن مسیج=========================*/
    public void show_msg(String msg_txt) {
        TSnackbar snackbar = TSnackbar.make(root, msg_txt, TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
