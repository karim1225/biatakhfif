package app.co.biatakhfif.biatakhfif.Class;

import android.app.Application;
import android.content.Context;
import app.co.biatakhfif.biatakhfif.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class G extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANYekanWeb.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
