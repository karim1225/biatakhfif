package app.co.biatakhfif.biatakhfif.WebService;

import app.co.biatakhfif.biatakhfif.Model.Web_res_data;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {


    /*==========user_controller==============*/
    @POST("user/login")
    @FormUrlEncoded
    Call<Web_res_data> login(@Field("data") String data);

    @POST("user/sing_up")
    @FormUrlEncoded
    Call<Web_res_data> sing_up(@Field("data") String data);

    @POST("user/send_sms")
    @FormUrlEncoded
    Call<Web_res_data> send_sms(@Field("data") String data);

    @POST("user/call_user")
    @FormUrlEncoded
    Call<Web_res_data> call_user(@Field("data") String data);

    @POST("user/remember_pass")
    @FormUrlEncoded
    Call<Web_res_data> remember_pass(@Field("data") String data);

    @POST("user/edit_user")
    @FormUrlEncoded
    Call<Web_res_data> edit_user(@Field("data") String data);




}
