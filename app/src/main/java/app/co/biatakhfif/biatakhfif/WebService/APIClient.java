package app.co.biatakhfif.biatakhfif.WebService;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import app.co.biatakhfif.biatakhfif.Class.TLSSocketFactory;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static String BASE_URL = "http://192.168.1.147/api/";
    public static String img_URL = "http://192.168.1.147/image/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        OkHttpClient client=new OkHttpClient();
        try {
            client = new OkHttpClient.Builder()
                    .sslSocketFactory(new TLSSocketFactory())
                    .build();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
