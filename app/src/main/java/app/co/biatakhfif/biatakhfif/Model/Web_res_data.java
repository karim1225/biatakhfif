package app.co.biatakhfif.biatakhfif.Model;

import com.google.gson.annotations.SerializedName;

public class Web_res_data {
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @SerializedName("data")

    private String data;
}
