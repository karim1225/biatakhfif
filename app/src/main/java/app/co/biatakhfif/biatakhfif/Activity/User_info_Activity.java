package app.co.biatakhfif.biatakhfif.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import app.co.biatakhfif.biatakhfif.Class.G;
import app.co.biatakhfif.biatakhfif.Class.SharedPref;
import app.co.biatakhfif.biatakhfif.Model.Web_res_data;
import app.co.biatakhfif.biatakhfif.R;
import app.co.biatakhfif.biatakhfif.WebService.APIClient;
import app.co.biatakhfif.biatakhfif.WebService.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class User_info_Activity extends AppCompatActivity {
    /*==========تعریف های لایه نمایش=========*/

    Toolbar toolbar;
    CoordinatorLayout root;
    ConstraintLayout panel_moshakhasat;
    EditText edt_user_name, edt_user_family, edit_user_mob, edit_user_pass, edt_pass_repit, edit_email;
    Button btn_send_data,btn_exit_account;
    ProgressBar progress_bar;
    /*=============متغییر های این صفحه==============*/

    JSONObject data_json, data = null;
    public static String user_id, user_mob, user_name, user_family, user_email, active_code;

    /*=================تغییر فونت======================*/
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);


        /*===========فراخونی تابع ها===========*/
        castha();
        toolbar_void();
        maghadir();
        clickha();
    }
    /*===========اجرای کد با کلید برگشت====================*/
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(G.context,MainActivity.class);
        startActivity(intent);
        finish();
    }
    /*==============کست ها و کلیک ها==============*/
    public void toolbar_void() {
        toolbar.setTitle("ویرایش اطلاعات کاربری");
        setSupportActionBar(toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setTextSize(16);
            }
        }

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
    public void castha() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        root = (CoordinatorLayout) findViewById(R.id.root);

        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        btn_send_data = (Button) findViewById(R.id.btn_send_data);
        btn_exit_account = (Button) findViewById(R.id.btn_exit_account);

        edit_user_mob = (EditText) findViewById(R.id.edit_user_mob);
        edt_user_name = (EditText) findViewById(R.id.edt_user_name);
        edt_user_family = (EditText) findViewById(R.id.edt_user_family);
        edit_user_pass = (EditText) findViewById(R.id.edit_user_pass);
        edt_pass_repit = (EditText) findViewById(R.id.edt_pass_repit);
        edit_email = (EditText) findViewById(R.id.edit_email);

        panel_moshakhasat = (ConstraintLayout) findViewById(R.id.panel_moshakhasat);

        SharedPref.init(this);


    }
    public void clickha() {
        btn_send_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_user_mob.getText().toString().equals("") || edt_user_name.getText().toString().equals("") || edit_user_pass.getText().toString().equals("") ||
                        edt_pass_repit.getText().toString().equals("") || edit_email.getText().toString().equals("")) {
                    show_msg(getResources().getString(R.string.msg_item_null));
                } else {
                    if (edit_user_pass.getText().toString().equals(edt_pass_repit.getText().toString())) {
                        progress_bar.setVisibility(View.VISIBLE);
                        panel_moshakhasat.setVisibility(View.INVISIBLE);

                        send_data_update(toBase64(data_json_cod_sin_up().toString()));


                    } else {
                        show_msg(getResources().getString(R.string.msg_no_pass_repit));
                    }
                }
            }
        });
        btn_exit_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress_bar.setVisibility(View.INVISIBLE);
                panel_moshakhasat.setVisibility(View.VISIBLE);
                SharedPref.write("user_mob","null");
                Toast.makeText(G.context, getResources().getString(R.string.msg_exit_account), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(G.context, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
    /*===============مقادیر اولیه====================*/
    public void maghadir(){
        edt_user_name.setText(SharedPref.read("user_name",""));
        edt_user_family.setText(SharedPref.read("user_family",""));
        edit_user_mob.setText(SharedPref.read("user_mob",""));
        edit_user_pass.setText(SharedPref.read("user_pass",""));
        edt_pass_repit.setText(SharedPref.read("user_pass",""));
        edit_email.setText(SharedPref.read("user_email",""));

    }
    /*====================اتصال به سرور=======================*/
    public void send_data_update(String data){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.edit_user(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {
                if(response.isSuccessful()){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);

                    SharedPref.write("user_name",edt_user_name.getText().toString());
                    SharedPref.write("user_family",edt_user_family.getText().toString());
                    SharedPref.write("user_pass",edit_user_pass.getText().toString());

                    Toast.makeText(G.context, getResources().getString(R.string.msg_data_ok), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(G.context, MainActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));
                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if (t instanceof IOException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                } else if (t instanceof TimeoutException) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_moshakhasat.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }

                progress_bar.setVisibility(View.INVISIBLE);
                panel_moshakhasat.setVisibility(View.VISIBLE);
                show_msg(getResources().getString(R.string.msg_error_res_data));

            }
        });

    }
    /*=========================کد و دیکد کردن اطلاعات====================*/
    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String fromBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject data_json_cod_sin_up() {
        data_json = new JSONObject();
        try {
            data_json.put("user_id", SharedPref.read("user_id",""));
            data_json.put("user_pass", edit_user_pass.getText().toString());
            data_json.put("user_name", edt_user_name.getText().toString());
            data_json.put("user_family", edt_user_family.getText().toString());


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }

    /*=================نمایش دادن مسیج=========================*/
    public void show_msg(String msg_txt) {
        TSnackbar snackbar = TSnackbar.make(root, msg_txt, TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
