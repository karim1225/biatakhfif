package app.co.biatakhfif.biatakhfif.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

import app.co.biatakhfif.biatakhfif.Class.G;
import app.co.biatakhfif.biatakhfif.Class.SharedPref;
import app.co.biatakhfif.biatakhfif.Model.Web_res_data;
import app.co.biatakhfif.biatakhfif.R;
import app.co.biatakhfif.biatakhfif.WebService.APIClient;
import app.co.biatakhfif.biatakhfif.WebService.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Singin_Activity extends AppCompatActivity {

    /*==========تعریف های لایه نمایش=========*/
    Toolbar toolbar;
    TextView btn_sin_up, btn_remmber_pass;
    EditText e_user_mob, e_user_pass;
    CheckBox ch_show_pass;
    Button btn_vorod;
    ProgressBar progress_bar;
    ConstraintLayout panel_sing_in;
    CoordinatorLayout root;

    /*=============متغییر های این صفحه==============*/
    JSONObject data_json, data = null;
    public static String user_id, user_mob, user_name, user_family, user_email,user_pass;


    /*===================تغییر فونت==================*/
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    /*========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singin);


        /*===========فراخونی تابع ها===========*/
        castha();
        toolbar_void();
        clickha();

    }
    /*===========اجرای کد با کلید برگشت====================*/
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(G.context,MainActivity.class);
        startActivity(intent);
        finish();
    }
    /*==============کست ها و کلیک ها==============*/


    public void toolbar_void() {
        toolbar.setTitle("ورود");
        setSupportActionBar(toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setTextSize(16);
            }
        }

        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void castha() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btn_sin_up = (TextView) findViewById(R.id.btn_sin_up);
        btn_remmber_pass = (TextView) findViewById(R.id.btn_remmber_pass);
        e_user_mob = (EditText) findViewById(R.id.edit_user_mob);
        e_user_pass = (EditText) findViewById(R.id.edit_user_pass);
        ch_show_pass = (CheckBox) findViewById(R.id.btn_show_pass);
        btn_vorod = (Button) findViewById(R.id.btn_vorod);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        panel_sing_in = (ConstraintLayout) findViewById(R.id.panel_sing_in);
        root = (CoordinatorLayout) findViewById(R.id.root);
        SharedPref.init(this);

    }

    public void clickha() {
        btn_sin_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Singup_Activity.class);
                startActivity(intent);
            }
        });
        btn_remmber_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(e_user_mob.getText().toString().equals("")){
                    show_msg(getResources().getString(R.string.msg_user_mob));
                }else {
                    progress_bar.setVisibility(View.VISIBLE);
                    panel_sing_in.setVisibility(View.INVISIBLE);
                    remember_pass(toBase64(data_json_cod_repit_pass().toString()));

                }

            }
        });
        ch_show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch_show_pass.isChecked()) {
                    e_user_pass.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    e_user_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                }
            }
        });
        btn_vorod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                login(toBase64(data_json_cod().toString()));
                progress_bar.setVisibility(View.VISIBLE);
                panel_sing_in.setVisibility(View.INVISIBLE);

            }
        });

    }


    /*====================اتصال به سرور=======================*/

    public void login(String data) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.login(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {
                if (response.isSuccessful()) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    JSONObject data = null;
                    String vall = fromBase64(response.body().getData());
                    try {
                        data = new JSONObject(vall);
                        user_id = data.get("id").toString();
                        SharedPref.write("user_id",user_id);

                        user_email = data.get("email").toString();
                        SharedPref.write("user_email",user_email);

                        user_name = data.get("user_name").toString();
                        SharedPref.write("user_name",user_name);

                        user_family = data.get("user_family").toString();
                        SharedPref.write("user_family",user_family);

                        user_pass = data.get("user_pass").toString();
                        SharedPref.write("user_pass", user_pass);

                        SharedPref.write("user_mob",e_user_mob.getText().toString());

                        Intent intent = new Intent(G.context, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } catch (JSONException e) {
                        //e.printStackTrace();
                        show_msg(getResources().getString(R.string.msg_error));
                    }


                } else if (response.code() == 300) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_data_not_ok));

                } else if (response.code() == 400) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_data_not_ok));

                } else {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));
                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if(t instanceof IOException){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                }else if(t instanceof TimeoutException){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }

                progress_bar.setVisibility(View.INVISIBLE);
                panel_sing_in.setVisibility(View.VISIBLE);
                show_msg(getResources().getString(R.string.msg_error_res_data));

            }
        });
    }

    public void remember_pass(String data){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<Web_res_data> call = apiInterface.remember_pass(data);
        call.enqueue(new Callback<Web_res_data>() {
            @Override
            public void onResponse(Call<Web_res_data> call, Response<Web_res_data> response) {
                if(response.isSuccessful()) {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_remember_pass));

                }else if(response.code()==300){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_no_user_mob));
                }else {
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_res_data));

                }

            }

            @Override
            public void onFailure(Call<Web_res_data> call, Throwable t) {
                if(t instanceof IOException){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_internet));
                }else if(t instanceof TimeoutException){
                    progress_bar.setVisibility(View.INVISIBLE);
                    panel_sing_in.setVisibility(View.VISIBLE);
                    show_msg(getResources().getString(R.string.msg_error_timout));
                }

                progress_bar.setVisibility(View.INVISIBLE);
                panel_sing_in.setVisibility(View.VISIBLE);
                show_msg(getResources().getString(R.string.msg_error_res_data));

            }
        });
    }


    /*=========================کد و دیکد کردن اطلاعات====================*/


    public static String toBase64(String message) {
        byte[] data;
        try {
            data = message.getBytes("UTF-8");
            String base64Sms = Base64.encodeToString(data, Base64.DEFAULT);
            return base64Sms;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String fromBase64(String message) {
        byte[] data = Base64.decode(message, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject data_json_cod() {
        data_json = new JSONObject();
        try {
            data_json.put("user_mob", e_user_mob.getText().toString());
            data_json.put("user_pass", e_user_pass.getText().toString());


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }
    public JSONObject data_json_cod_repit_pass() {
        data_json = new JSONObject();
        try {
            data_json.put("user_mob", e_user_mob.getText().toString());


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            //  e.printStackTrace();
            return null;
        }
        return data_json;
    }

    /*=================نمایش دادن مسیج=========================*/
    public void show_msg(String msg_txt) {
        TSnackbar snackbar = TSnackbar.make(root, msg_txt, TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


}
